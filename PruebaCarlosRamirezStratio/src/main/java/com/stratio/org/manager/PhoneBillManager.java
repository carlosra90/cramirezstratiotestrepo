package com.stratio.org.manager;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.stratio.org.domain.PhoneBill;
import com.stratio.org.domain.PhoneBillResponse;
import com.stratio.org.util.ResponseCodeEnum;
import com.stratio.org.util.Validator;

@Service
public class PhoneBillManager {
	private static final Logger logger = LoggerFactory.getLogger(PhoneBillManager.class);
	@Autowired
	Validator validator;
	@Value("${application.end_of_line_regex}")
	private String endOfLine;
	@Value("${application.line_regex}")
	private String lineRegex;
	@Value("${application.time_regex}")
	private String timeRegex;
	@Value("${application.phone_regex}")
	private String phoneRegex;

	public PhoneBillResponse getPhoneBillCalculation(PhoneBill phoneLog) {
		logger.debug("{}", phoneLog);
		PhoneBillResponse response = new PhoneBillResponse();
		response.setCode(ResponseCodeEnum.OK.getCode());
		response.setDetail(ResponseCodeEnum.OK.getResponse());
		ResponseCodeEnum validation = validator.validateNotNull(phoneLog);
		String[] logLines = phoneLog.getCallLog().split(endOfLine);
		validation = validator.validatePhoneLog(logLines,validation);
		Map<String,Integer> cellphoneDutarion;
		if (validation.compareTo(ResponseCodeEnum.SUCCESS_VALIDATION) == 0) {
			cellphoneDutarion= new HashMap<>();
			DateTimeFormatter formater = DateTimeFormat.forPattern("HH:mm:ss");
			for(int i =0;i < logLines.length;i++) {
				String[] traceLine = logLines[i].split(",");
				DateTime time = DateTime.parse(traceLine[0], formater);
				incrementDuration(cellphoneDutarion, time,traceLine);
			}
			response.setAmount(obtainMapAmount(cellphoneDutarion));
			
		} else {
			response.setCode(validation.getCode());
			response.setDetail(validation.getResponse());
		}
		return response;
	}
	/**
	 * obtain the amount for each phone
	 * @param cellphoneDutarion
	 * @return
	 */
	private double obtainMapAmount(Map<String, Integer> cellphoneDutarion) {
		double declaredAmount=0;
		String phoneToDiscount=null;
		Integer secondsToDiscount=0;
		double amount=0;
		for(Map.Entry<String, Integer> entry : cellphoneDutarion.entrySet()) {
			amount=getAmount(amount, entry.getValue(),entry.getKey());
			if(entry.getValue().intValue() > secondsToDiscount.intValue()) {
				phoneToDiscount = entry.getKey();
				secondsToDiscount = entry.getValue();
			}else if(entry.getValue().intValue() == secondsToDiscount.intValue()) {
				phoneToDiscount = obtainTheLowestPhone(entry.getKey(),phoneToDiscount);
			}
		}
		declaredAmount=obtainTotalAmountWithCharges(amount, secondsToDiscount,phoneToDiscount+"-Dto");
		logger.info("[Total amount to pay without discount= {}][Total amount to pay with discount={}][ the free promotion applies to phone {}]",amount,declaredAmount,phoneToDiscount);
		return declaredAmount;
	}

	/**
	 * obtain the final amount less discount
	 * @param amount
	 * @param secondsToDiscount
	 * @return
	 */
	private double obtainTotalAmountWithCharges(double amount, Integer secondsToDiscount,String phone) {
		double discount= getAmount(0, secondsToDiscount,phone);
		return amount -discount;
		
	}
	
	/**
	 * obtain the amount value per call
	 * @param amount
	 * @param secondsToDiscount
	 * @return
	 */
	private double getAmount(double amount, Integer secondsToDiscount,String phone) {
		if(secondsToDiscount< 300) {
			amount += secondsToDiscount*3;
			logger.debug("[seconds_from'{}'={}]",phone,secondsToDiscount);
		}else {
			double seconds= secondsToDiscount;
			BigDecimal totalOfMinutes= new BigDecimal((seconds/60.d));
			totalOfMinutes = totalOfMinutes.setScale(0, BigDecimal.ROUND_UP);
			logger.debug("[seconds_from'{}'={}][total_minutes={}]",phone,secondsToDiscount,totalOfMinutes);
			amount += totalOfMinutes.doubleValue()*150;
		}
		return amount;
	} 

	/**
	 * obtain the tie number for discount
	 * @param key
	 * @param phoneToDiscount
	 * @return
	 */
	private String obtainTheLowestPhone(String key, String phoneToDiscount) {
		String keyToReturn = key;
		Long keyPhone= Long.parseLong(key.replaceAll("-", ""));
		Long actualPhone = Long.parseLong(null != phoneToDiscount? phoneToDiscount.replaceAll("-", ""):null);
		if(null != actualPhone) {
			if(keyPhone > actualPhone) {
				keyToReturn =phoneToDiscount;
			}
		}
			return keyToReturn;
	}

	/**
	 * increase 1 by 1 the duration 
	 * @param cellphoneDutarion
	 * @param time
	 * @param traceLine
	 */
	private void incrementDuration(Map<String, Integer> cellphoneDutarion, DateTime time, String[] traceLine) {
		Integer duration = cellphoneDutarion.get(traceLine[1]);
		if(null != duration) {
			duration+= time.getSecondOfDay();
		}else {
			duration = time.getSecondOfDay();
		}
		cellphoneDutarion.put(traceLine[1], duration);
		
	}

}
