package com.stratio.org.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.stratio.org.domain.PhoneBill;

@Component
public class Validator {
	private static final Logger logger = LoggerFactory.getLogger(Validator.class);
	@Value("${application.line_regex}")
	private String lineRegex;
	@Value("${application.time_regex}")
	private String timeRegex;
	@Value("${application.phone_regex}")
	private String phoneRegex;
	
	public ResponseCodeEnum validateNotNull(PhoneBill phoneLog) {
		ResponseCodeEnum codeEnum =ResponseCodeEnum.SUCCESS_VALIDATION; 
		try {
			Assert.notNull(phoneLog,"request is not initializate");
			Assert.notNull(phoneLog.getCallLog(),"log trace is null");
		} catch (Exception e) {
			logger.error("{} [ERROR={}]",phoneLog, e);
			codeEnum = ResponseCodeEnum.FAILED_VALIDATION;
			codeEnum.setResponse(codeEnum.getResponse() + "-" +e);
		}
		return codeEnum;
	}
	
	public  ResponseCodeEnum validatePhoneLog(String[] logLines,ResponseCodeEnum codeEnum) {
		
		try {
			Assert.isTrue((logLines.length <= 100),String.format("logs lines y major than 100 ->[%d registries]", logLines.length));
			for(int i = 0; i < logLines.length; i++) {
				Assert.isTrue(logLines[i].matches(lineRegex), String.format("phone bill log line [%s] is not valid iterator [%d]",logLines[i],i));
				String[] traceLine = logLines[i].split(",");
				Assert.isTrue(traceLine[0].matches(timeRegex), String.format("phone bill log time [%s] is not valid iterator [%d]",traceLine[0],i));
				Assert.isTrue(traceLine[1].matches(phoneRegex), String.format("phone bill log phone [%s] is not valid iterator [%d]",traceLine[1],i));
			}
		} catch (Exception e) {
			logger.error("{} [ERROR={}]",logLines, e);
			codeEnum = ResponseCodeEnum.FAILED_VALIDATION;
			codeEnum.setResponse(codeEnum.getResponse() + "-" +e);
		}
		
		return codeEnum;
	}
}
