package com.stratio.org.util;

public enum ResponseCodeEnum {

	OK("200","OK"),
	SUCCESS_VALIDATION("202","Validation successful"),
	FAILED_VALIDATION("500","An error has been occurred");

	private String code;
	private String response;

	private ResponseCodeEnum(String code, String response) {
		this.code = code;
		this.response = response;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	
}
