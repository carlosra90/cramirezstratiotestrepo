package com.stratio.org.domain;

public class PhoneBill {
	private String callLog;

	public String getCallLog() {
		return callLog;
	}

	public void setCallLog(String callLog) {
		this.callLog = callLog;
	}

	@Override
	public String toString() {
		return "PhoneBill [callLog=" + callLog + "]";
	}
	
	
}
