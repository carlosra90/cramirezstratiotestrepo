package com.stratio.org.domain;

public class PhoneBillResponse {

	private String code;
	private String detail;
	private double amount;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "PhoneBillResponse [code=" + code + ", detail=" + detail + ", amount=" + amount + "]";
	}
	
	
}
