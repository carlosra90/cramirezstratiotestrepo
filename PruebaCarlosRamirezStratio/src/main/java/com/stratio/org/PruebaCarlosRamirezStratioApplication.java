package com.stratio.org;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PruebaCarlosRamirezStratioApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaCarlosRamirezStratioApplication.class, args);
	}
}
