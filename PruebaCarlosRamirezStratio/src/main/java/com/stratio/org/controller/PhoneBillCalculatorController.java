package com.stratio.org.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.stratio.org.domain.PhoneBill;
import com.stratio.org.domain.PhoneBillResponse;
import com.stratio.org.manager.PhoneBillManager;

@RestController
public class PhoneBillCalculatorController {

	@Autowired
	private PhoneBillManager manager;
	private static final Logger logger = LoggerFactory.getLogger(PhoneBillCalculatorController.class);
	
	@RequestMapping(value = "/calculatePhoneBill", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE })
	public PhoneBillResponse getPhoneBillCalculation(@RequestBody PhoneBill phoneLog) {
		logger.info("[phone_bill_received]{}",phoneLog);
		return manager.getPhoneBillCalculation(phoneLog);
	}
}
